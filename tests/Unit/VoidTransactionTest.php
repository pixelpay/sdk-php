<?php

namespace PixelPay\Sdk\Tests\Unit;

use PHPUnit\Framework\TestCase;
use PixelPay\Sdk\Requests\VoidTransaction;
use PixelPay\Sdk\Base\Helpers;

class VoidTransactionTest extends TestCase
{
	public function testVoidSignature()
	{
		$void_tx = new VoidTransaction();
		$void_tx->void_signature = '357516ca6bba30771e301c4285b5d699c47e9ac050151e592b37b748555dd247';

		$void_json = json_decode($void_tx->toJson(), true);

		$this->assertEquals(Helpers::hash('SHA256', 'sonia@pixel.hn|20230530|@s4ndb0x-abcd-1234-n1l4-p1x3l'), $void_json['void_signature']);
	}
}
