<?php

namespace PixelPay\Sdk\Tests\Unit;

use PHPUnit\Framework\TestCase;
use PixelPay\Sdk\Requests\SaleTransaction;

class SaleTransactionTest extends TestCase
{
	public function testPaymentTransactionInstallment()
	{
		$sale = new SaleTransaction();
		$sale->setInstallment(12, 'extra');

		$sale_json = json_decode($sale->toJson(), true);

		$this->assertEquals('extra', $sale_json['installment_type']);
		$this->assertEquals('12', $sale_json['installment_months']);
	}

	public function testPaymentTransactionPointsRedeem()
	{
		$sale = new SaleTransaction();
		$sale->withPointsRedeemAmount(200);

		$sale_json = json_decode($sale->toJson(), true);

		$this->assertEquals('200.00', $sale_json['points_redeem_amount']);
	}
}
