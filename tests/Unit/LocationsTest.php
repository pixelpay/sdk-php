<?php

namespace PixelPay\Sdk\Tests\Unit;

use PHPUnit\Framework\TestCase;
use PixelPay\Sdk\Resources\Locations;

class LocationsTest extends TestCase
{
	public function testCountriesList()
	{
		$this->assertTrue(!empty(Locations::countriesList()));
		$this->assertEquals('Honduras', Locations::countriesList()['HN']);
	}

	public function testStatesList()
	{
		$this->assertTrue(!empty(Locations::statesList('HN')));
		$this->assertEquals('Cortes', Locations::statesList('HN')['HN-CR']);
		$this->assertEquals('Guatemala', Locations::statesList('GT')['GT-01']);
		$this->assertEquals('Costa Caribe Norte', Locations::statesList('NI')['NI-AN']);
		$this->assertEquals('Lancashire', Locations::statesList('GB')['GB-LAN']);
	}

	public function testFormatList()
	{
		$this->assertTrue(!empty(Locations::formatsList('HN')));
		$this->assertEquals(504, Locations::formatsList('HN')['phone_code']);
		$this->assertEquals('', Locations::formatsList('HN')['zip_format']);
	}
}
